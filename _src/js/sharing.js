/* sharing BEGIN */
(function(){
    var popups = function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    };

    function share(){
        var tmp = 0,
            url = '',
            param = {
                purl: 'http://goodini-juice.ru.heagaf.t-agency.ru/',
                ptitle: "Goodini - так полезное становится вкусным.",
                text: "Поистине шедевр миксологии. При необычайно приятном ягодном вкусе он обогащен микро- и макроэлементами из полезных овощей и шпината. В легко усваиваемой форме здесь содержатся биологически активные вещества, которые поступают только через пищу.",
                img: "http://goodini-juice.ru.heagaf.t-agency.ru/img/main/bottle.png"
            };
        return {
            vkontakte: function() {
                url  = 'http://vk.com/share.php?';
                url += 'url='          + encodeURIComponent(param.purl);
                url += '&title='       + encodeURIComponent(param.ptitle);
                url += '&description=' + encodeURIComponent(param.text);
                url += '&image=' + encodeURIComponent(param.img);
                url += '&noparse=true';
                popups(url);
            },
            odnoklassniki: function() {
                url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st.comments=' + encodeURIComponent(param.text);
                url += '&st._surl='    + encodeURIComponent(param.purl);
                popups(url);
            },
            facebook: function() {
                url  = 'http://www.facebook.com/sharer.php?s=100';
                url += '&p[url]='       + encodeURIComponent(param.purl+'s');
                //                url += '&p[url]='       + encodeURIComponent('http://riderosan.com');
                url += '&p[images][0]=' + encodeURIComponent(param.img);
                url += '&p[title]='     + encodeURIComponent(param.ptitle);
                url += '&p[summary]='   + encodeURIComponent(param.text);
                popups(url);
//                console.log("http://"+document.location.hostname+"?link=/"+document.location.hash.replace("#!/","")+"/"+document.location.hash);
            },
            twitter: function() {
                url  = 'http://twitter.com/share?';
                url += 'text='      + encodeURIComponent(param.ptitle);
                url += '&url='      + encodeURIComponent(param.purl);
                url += '&counturl=' + encodeURIComponent(param.purl);
                popups(url);
            }
        }
    }

    $(".wrapper").on("click", ".header_soc_link", function(e){
        e.preventDefault();
        if($(this).hasClass("vk")){
            share().vkontakte();
        } else if($(this).hasClass("tw")){
            share().twitter();
        } else if($(this).hasClass("fb")){
            share().facebook();
        } else if($(this).hasClass("ok")){
            share().odnoklassniki();
        }
    });

})();
/* sharing END */