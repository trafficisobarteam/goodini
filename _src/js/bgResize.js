//--------------------------------------background resize feature BEGIN

function bgResize(wrapper, imgs){
    var win = wrapper.eq(0),
        winWidth = win.width(),
        winHeight = win.height(),
        sizes = [];
    imgs.each(function(){
        sizes.push([$(this).width(), $(this).height()]);
    });
    $(window).on("resize", function(){
        winWidth = win.width();
        winHeight = win.height();
        $(".header_slider_item").css({
            'maxHeight': $(window).height() - 240,
            'maxWidth': $(window).width() - 100
        });
        imgs.each(function(indx){
            var img = $(this),
                imgWidth = sizes[indx][0],
                imgHeight = sizes[indx][1],
                newHeight = 0,
                newWidth = 0,
                newLeft = 0,
                newTop = 0;
            if(imgWidth/winWidth > imgHeight/winHeight){
                newHeight = winHeight;
                newWidth = imgWidth*winHeight/(imgHeight);
                newLeft = (winWidth-newWidth)/2;
            } else {
                newWidth = winWidth;
                newHeight = imgHeight*winWidth/(imgWidth);
                newTop = (winHeight-newHeight)/2;
            }
            img.css({
                width: newWidth,
                height: newHeight,
                left: newLeft,
                top: newTop
            });
        });
    });
    $(window).trigger("resize");
}
function videoBgResize(wrapper, imgs){
    var win = wrapper.eq(0),
        winWidth = win.width(),
        winHeight = win.height(),
        sizes = [];
    imgs.each(function(){
        sizes.push([($(this).width()), ($(this).height())]);
    });
    $(window).on("resize", function(){
        winWidth = win.width();
        winHeight = win.height();
        imgs.each(function(indx){
            var img = $(this),
                imgWidth = sizes[indx][0],
                imgHeight = sizes[indx][1],
                newHeight = 0,
                newWidth = 0,
                newLeft = 0,
                newTop = 0;
            if(imgWidth/winWidth > imgHeight/winHeight){
                newHeight = winHeight;
                newWidth = imgWidth*winHeight/(imgHeight);
                newLeft = (winWidth-newWidth)/2;
            } else {
                newWidth = winWidth;
                newHeight = imgHeight*winWidth/(imgWidth);
                newTop = (winHeight-newHeight)/2;
            }
            newWidth = newWidth*1.4;
            newHeight = newHeight*1.4;
            newLeft = (winWidth-newWidth)/2;
            newTop = (winHeight-newHeight)/2;

            img.css({
                width: newWidth,
                height: newHeight,
                left: newLeft,
                top: newTop
            });
        });
    });
    $(window).trigger("resize");
}
function bgResizeRev(wrapper, imgs){
    var win = wrapper.eq(0),
        winWidth = win.width(),
        winHeight = win.height(),
        sizes = [];
    imgs.each(function(){
        sizes.push([$(this).width(), $(this).height()]);
    });
    $(window).on("resize", function(){
        winWidth = win.width();
        winHeight = win.height();
        imgs.each(function(indx){
            var img = $(this),
                imgWidth = sizes[indx][0],
                imgHeight = sizes[indx][1],
                newHeight = 0,
                newWidth = 0,
                newLeft = 0,
                newTop = 0;
            if(imgWidth/winWidth < imgHeight/winHeight){
                newHeight = winHeight;
                newWidth = imgWidth*winHeight/(imgHeight);
                newLeft = (winWidth-newWidth)/2;
            } else {
                newWidth = winWidth;
                newHeight = imgHeight*winWidth/(imgWidth);
                newTop = (winHeight-newHeight)/2;
            }
            img.css({
                width: newWidth,
                height: newHeight,
                left: newLeft,
                top: newTop
            });
        });
    });
    $(window).trigger("resize");
}


(function(){
    $(window).load(function(){
        bgResize($(".maincontent"), $(".page_bg img"));
    });
})();

//--------------------------------------background resize feature END