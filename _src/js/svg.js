/**
 * Created by vatiba01 on 06.11.2014.
 */
(function(){
    if($("html.svg").length){
        $(".main_right_arrow").remove();
        $(".main_content_right .main_right_wrap").prepend('' +
        '<svg class="main_right_arrow"' +
        'version="1.1"' +
        'xmlns="http://www.w3.org/2000/svg"' +
        'xmlns:xlink="http://www.w3.org/1999/xlink"' +
        'width="63px" height="10px"' +
        'viewBox="0 0 63 10" preserveAspectRatio="none">' +
        '<g>' +
        '            <image width="63" height="10" xlink:href="data:image/png;base64,' +
        'iVBORw0KGgoAAAANSUhEUgAAAD8AAAAKAQMAAAFOISQUAAAABlBMVEW9uK+9uK+RfwRoAAAAAXRS' +
        'TlMAQObYZgAAADRJREFUCNdjYACCBiD8/x/EgpAJIMIBRCjAWf/////HcAAuK8MABQUwxn8I+Mdg' +
        'ARPhgdIA7yYT38x5MJgAAAAASUVORK5CYII=' +
        '"/>' +
        '</g>' +
        '</svg>' +
        '');
    }
})();