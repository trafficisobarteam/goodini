/**
 * Created by Verdi on 12.11.2014.
 */
(function(){
    $(".catalog_info_link").on("click", function(e){
        e.preventDefault();
        var elem = $(this),
            href = elem.attr("href"),
            id = $(href.replace("#popup_","#catalog_popup_")),
            popup = $(".catalog_popup_list");
        if(id.length){
            popup.show();
            $(".catalog_popup_item").removeClass("active");
            id.addClass("active");
        }
    });
    $(".catalog_popup_close").on("click", function(e){
        e.preventDefault();
        $(".catalog_popup_list").hide();
        $(".catalog_popup_item").removeClass("active");
    });
})();
