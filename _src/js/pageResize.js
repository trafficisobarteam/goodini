//-----------------------------Ресайз всея begin
(function(){
    function pageResize(data){
        if(typeof data.normalSize == "undefined"){
            data.normalSize = {
                width: 1400,
                height: 900
            }
        }
        if(typeof data.normalFont == "undefined"){
            data.normalFont = 12;
        }
        var win = data.wrapper,
            winWidth = win.width(),
            winHeight = win.height(),
            normalWidth = data.normalSize.width,
            normalHeight = data.normalSize.height,
            ratio = normalWidth/normalHeight,
            normalSize = data.normalFont,
            perc = 1,
            elem = data.resizeElem;
        $(window).resize(function(){
            winWidth = win.width();
            winHeight = win.height();
            var tmpHeight = 0,
                tmpWidth = 0;
            if(ratio < winWidth/winHeight){
                tmpHeight = winHeight;
                tmpWidth = tmpHeight * ratio;

            } else {
                tmpWidth = winWidth;
                tmpHeight = tmpWidth / ratio;
            }

            perc = tmpWidth/normalWidth;
            var newSize = normalSize*perc;
            elem.css({
                fontSize: newSize
            });
            if(newSize < 12){
                win.addClass("smallSize");
            } else {
                win.removeClass("smallSize");
            }
        });
        $(window).trigger("resize");
    }

    pageResize({
        wrapper: $(".maincontent"),
        resizeElem: $(".maincontent"),
        normalSize: {
            width: 1840,
            height: 960
        },
        normalFont: 16
    });

})();

//-----------------------------Ресайз всея end


